# TP 1


## Lancer les containeurs actifs:

Pour lancer le container il faut taper la commande suivante:
```
[vagrant@centos7 ~]$ docker run alpine sleep 9999
```

Et pour lister les containers lancé:
```
[vagrant@centos7 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND        CREATED              STATUS              PORTS     NAMES
31df9b964683   alpine    "sleep 9999"   About a minute ago   Up About a minute             boring_archimedes
```

Nous pouvons donc voir que l'ID est `31df9b964683` et son nom `boring_archimedes`


## mettre en evidence d'une partie isolation:

Pour montrer que la vm et le container sont bien isoler, voici les différentes raisons:

### L'arborescence
En faisant `ps`
```
[vagrant@centos7 ~]$ ps
  PID TTY          TIME CMD
  26546 pts/1    00:00:00 bash
  27082 pts/1    00:00:00 ps
```
```
/ # ps
PID   USER     TIME  COMMAND
  1   root      0:00 sleep 9999
 30   root      0:00 sh
 36   root      0:00 ps
 ```

 ### Les cartes réseaux
 ```
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
    valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:19:b4:38 brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global noprefixroute dynamic eth0
    valid_lft 78330sec preferred_lft 78330sec
3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:65:38:f0:25 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 brd 172.17.255.255 scope global docker0
    valid_lft forever preferred_lft forever
13: vethd56b81f@if12: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether c6:32:0e:7f:24:8f brd ff:ff:ff:ff:ff:ff link-netnsid 0
 ```
 ```
/ # ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
12: eth0@if13: <BROADCAST,MULTICAST,UP,LOWER_UP,M-DOWN> mtu 1500 qdisc noqueue state UP 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
 ```

### Les utilisateurs des différents systèmes:
```
[vagrant@centos7 ~]$ cat /etc/passwd | awk -F: '{print$1}'
root
bin
daemon
adm
lp
sync
shutdown
halt
mail
operator
games
ftp
nobody
systemd-network
dbus
polkitd
sshd
postfix
chrony
vagrant
vboxadd
```
```
/ # cat /etc/passwd | awk -F: '{print$1}'
root
bin
daemon
adm
lp
sync
shutdown
halt
mail
news
uucp
operator
man
postmaster
cron
ftp
sshd
at
squid
xfs
games
cyrus
vpopmail
ntp
smmsp
guest
nobody
```

### Les différents points de montage:
sur la vm:
```
[vagrant@centos7 ~]$ df -h
Filesystem                       Size  Used Avail Use% Mounted on
devtmpfs                         908M     0  908M   0% /dev
tmpfs                            919M     0  919M   0% /dev/shm
tmpfs                            919M   17M  903M   2% /run
tmpfs                            919M     0  919M   0% /sys/fs/cgroup
/dev/mapper/centos_centos7-root   50G  2.0G   49G   4% /
/dev/sda1                       1014M  131M  884M  13% /boot
tmpfs                            184M     0  184M   0% /run/user/1000
```

Sur le container:
```
/ # df -h
Filesystem                Size      Used Available Use% Mounted on
overlay                  50.0G      2.0G     48.0G   4% /
tmpfs                    64.0M         0     64.0M   0% /dev
tmpfs                   918.9M         0    918.9M   0% /sys/fs/cgroup
shm                      64.0M         0     64.0M   0% /dev/shm
/dev/mapper/centos_centos7-root
                         50.0G      2.0G     48.0G   4% /etc/resolv.conf
/dev/mapper/centos_centos7-root
                         50.0G      2.0G     48.0G   4% /etc/hostname
/dev/mapper/centos_centos7-root
                         50.0G      2.0G     48.0G   4% /etc/hosts
tmpfs                   918.9M         0    918.9M   0% /proc/acpi
tmpfs                    64.0M         0     64.0M   0% /proc/kcore
tmpfs                    64.0M         0     64.0M   0% /proc/keys
tmpfs                    64.0M         0     64.0M   0% /proc/timer_list
tmpfs                    64.0M         0     64.0M   0% /proc/timer_stats
tmpfs                    64.0M         0     64.0M   0% /proc/sched_debug
tmpfs                   918.9M         0    918.9M   0% /proc/scsi
tmpfs                   918.9M         0    918.9M   0% /sys/firmware
```

## Detruire le container:
Il faut d'abord l'arreter (ça marche aussi avec l'ID):
```
docker stop boring_archimedes
```
Et arpès on la détruit:
```
docker rm boring_archimedes
```

## Lancer un container NGINX:
```
[vagrant@centos7 ~]$ docker run -d nginx -p 80
```

## Lancer un container NGINX
```
[vagrant@centos7 ~]$ docker run --name nginx_part1 -d -p 80:80 nginx
eb7833bc40b622619dffafac0e5a43ed6b43036d17e73f588d0ebc3f0be03579

[vagrant@centos7 ~]$ docker ps
CONTAINER ID   IMAGE     COMMAND                  CREATED         STATUS         PORTS                NAMES
eb7833bc40b6   nginx     "/docker-entrypoint.…"   3 seconds ago   Up 2 seconds   0.0.0.0:80->80/tcp   nginx_part1
```
Pour vérifier si ça fonctionne il suffit de faire cette commande sur l'hote:
```
lmaury@cheeki-breeki ~$ curl 192.168.20.21
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href="http://nginx.org/">nginx.org</a>.<br/>
Commercial support is available at
<a href="http://nginx.com/">nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```
Ou aussi d'entrer l'adresse IP de la VM sur le navigateur.

## 2. Gestion d'images
### récuperer l'image docker Apache en version 2.2
Il faut aller sur le docker hub, chercher la bonne image est une fois que c'est fait il suffit de taper:
```
[vagrant@centos7 ~]$ docker pull httpd:2.2
2.2: Pulling from library/httpd
f49cf87b52c1: Pull complete 
24b1e09cbcb7: Pull complete 
8a4e0d64e915: Pull complete 
bcbe0eb4ca51: Pull complete 
16e370c15d38: Pull complete 
Digest: sha256:9784d70c8ea466fabd52b0bc8cde84980324f9612380d22fbad2151df9a430eb
Status: Downloaded newer image for httpd:2.2
docker.io/library/httpd:2.2
[vagrant@centos7 ~]$ docker image ls
REPOSITORY   TAG       IMAGE ID       CREATED       SIZE
nginx        latest    f6d0b4767a6c   2 days ago    133MB
httpd        2.2       e06c3dbbfe23   2 years ago   171MB
```

### Création d'une image

Le Dockerfile:
```dockerfile
FROM alpine:3.12

RUN apk add --update --no-cache python3 && ln -sf python3 /usr/bin/python

EXPOSE 8888

WORKDIR /server.d/
COPY ./http.d/ /server.d/

CMD python3 -m http.server 8888
```


Build l'image:
```
[vagrant@centos7 python_web_server]$ docker build -f Dockerfile -t alpine-python3 .
Successfully built 309156ccd2ad
Successfully tagged alpine-python3:latest

[vagrant@centos7 python_web_server]$ docker image ls
REPOSITORY       TAG       IMAGE ID       CREATED         SIZE
alpine-python3   latest    309156ccd2ad   4 seconds ago   71.8MB
```

Pour lancer le container:
```
[vagrant@centos7 python_web_server]$ docker run --name alpython-sw -d -p 80:8888 alpine-python3:latest
```

Pour vérifier si tout est bon il suffit de se rendre sur le navigateur en rentrant l'IP de la VM comme la première fois mais cette fois-ci il y aura juste les fichiers du dossier affichés (sauf si il y a un fichier index.html évidemment).


Utiliser l'option `-v` de `docker run`
```
[vagrant@centos7 python_web_server]$ mkdir nouveau-dossier

[vagrant@centos7 python_web_server]$ docker run --name alpython-WS -v nouveau-dossier:/server.d/ -it -p 80:8888 alpine-python3
Serving HTTP on 0.0.0.0 port 8888 (http://0.0.0.0:8888/) ...
192.168.20.1 - - [14/Jan/2021 18:43:29] "GET / HTTP/1.1" 200 -
192.168.20.1 - - [14/Jan/2021 18:43:30] "GET / HTTP/1.1" 200 -
```
*Les deux dernière lignes, c'est parce que j'ai raffraichit la page*

## 3. Manipulation du démon docker
### Modifier le socket du Démon Docker:
Trouver le chemin:
```
[vagrant@centos7 ~]$ sudo find / -name docker.sock
/run/docker.sock
```

Il faut changer le fichier `/usr/lib/systemd/system/docker.service` et remplacer la ligne `ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock`.

Par `ExecStart=/usr/bin/dockerd -H tcp://0.0.0.0:6679 --containerd=/run/containerd/containerd.sock`

Le fait de faire mettre en IP le `0.0.0.0` permettra d'être vu par tous les appareils du réseaux.

#### Prouver que ça marche
```
lmaury@cheeki-breeki ~$ docker -H tcp://192.168.20.21:6679 image ls
REPOSITORY       TAG       IMAGE ID       CREATED        SIZE
alpine-python3   latest    309156ccd2ad   16 hours ago   71.8MB
nginx            latest    f6d0b4767a6c   3 days ago     133MB
alpine           3.12      389fef711851   4 weeks ago    5.58MB
alpine           latest    389fef711851   4 weeks ago    5.58MB
httpd            2.2       e06c3dbbfe23   2 years ago    171MB
```


### Le OMM score du démon Docker
Le core OMM du démon Docker est la valeur limite à laquelle on va tolerer le "dépassement de la mémoire" (*O*out *O*f *M*emory). Cette fonctionnalitée va permettre d'arrêter les processus qui dépassent cette valeur.

Il faut donc attribuer la valeur en fonction des besoin.

Si on ne veut utiliser que 100Mo de RAM il suffira d'utiliser l'otpion `-m` suivi de la valeur `100m` au moment du `docker run`

```
[vagrant@centos7 ~]$ docker run --name alpython-sw1 -m 100m -d -p 80:8888 alpine-python3:latest
```

Ainsi, le container sera arreté si il utilise plus de 100m.

## II docker-compose
### Ecrire un docker-compose-v1.yml
```yml
version: '3.8'

services:
    server_1:
        image: python_app:latest
        networks:
            - web_server_net
        ports:
            - "8888:8888"
        volumes: 
            - ./dossier-server1:/server.d/

    redis:
        image: redis
        networks:
            - web_server_net

networks:
    web_server_net:

```

### Ajouter un deuxième container

