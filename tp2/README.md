# TP 2: Orchestration de conteneurs et environnements flexibles

## 1) Lab setup
### Utiliser des commandes docker afin de créer votre cluster Swarm

`docker swarm init` sur le permier noeud:
```
[vagrant@node1 ~]$ docker swarm init --advertise-addr 192.168.20.21
Swarm initialized: current node (m6jo8x3g76ve7g3v1703w3cs7) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0ftzdmye1ukpml2fefo9okd41ivdmy64vr4n8wm3832qevtiyc-99w1p3qbcklb7owuggml2l8qi 192.168.20.21:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.
```

Se connecter vers le node1:
```
docker swarm join --token SWMTKN-1-4r28cnuvbt1x9jl87a1w1wr6r1dk9snbu7w5qf0v6krfni965j-cjui37r8hx4v4saiusrv9y5n8 node1.b3:2377
```

Afficher les différents noeuds:
```
[vagrant@node1 ~]$ docker node ls
ID                            HOSTNAME   STATUS    AVAILABILITY   MANAGER STATUS   ENGINE VERSION
i1a7bm8pzt5afcs28phw396f2 *   node1.b3   Ready     Active         Leader           20.10.2
xemvqvx3hlk223kahln0aecsi     node2.b3   Ready     Active         Reachable        20.10.2
oux57e4nqy2mqgd372n2w1tw1     node3.b3   Ready     Active         Reachable        20.10.2
```


## 2) Une Première application orchestrée
Réutiliser le docker-compose.yml du TP1
```yaml
version: '3.8'

services:
    server_1:
        image: python_app:latest
        networks:
            - web_server_net
        ports:
            - "8888:8888"

    redis:
        image: redis
        networks:
            - web_server_net

networks:
    web_server_net:
```


Puis le deployer:
```
[vagrant@node1 python-docker]$ docker stack deploy -c docker-compose.yml pyapp
Creating network pyapp_web_server_net
Creating service pyapp_server_1
Creating service pyapp_redis

[vagrant@node1 python-docker]$ docker service ls
ID             NAME             MODE         REPLICAS   IMAGE               PORTS
pgahl5tscyv0   pyapp_redis      replicated   1/1        redis:latest        
049l9xc7kskf   pyapp_server_1   replicated   1/1        python_app:latest   *:8888->8888/tcp
```

Et pour vérifier si tout fonctionne bien:
```
[vagrant@node2 python-docker]$ curl node1.b3:8888
<h1>Add key</h1>
<form action="/add" method = "POST">

Key:
<input type="text" name="key" >

Value:
<input type="text" name="value" >

<input type="submit" value="Submit">
</form>

<h1>Check key</h1>
<form action="/get" method = "POST">

Key:
<input type="text" name="key" >
<input type="submit" value="Submit">
</form>

```


Une fois fait, il n'y a plus qu'à faire la commande `docker scale`:
```
[vagrant@node1 python-docker]$ docker service scale pyapp_server_1=3
pyapp_server_1 scaled to 3
overall progress: 3 out of 3 tasks 
1/3: running   [==================================================>] 
2/3: running   [==================================================>] 
3/3: running   [==================================================>] 
verify: Service converged 

[vagrant@node1 python-docker]$ docker service ls
ID             NAME             MODE         REPLICAS   IMAGE               PORTS
pgahl5tscyv0   pyapp_redis      replicated   1/1        redis:latest        
049l9xc7kskf   pyapp_server_1   replicated   3/3        python_app:latest   *:8888->8888/tcp
```
Puis il ne reste plus qu'à vérifier si il y a bien Trois réplicats différents:
```
[vagrant@node1 python-docker]$ for (( i=3; i >= 1; i-- )); do curl -s node$i.b3:8888 | grep 'Host : '; done
Host : 6bdd73eec37b
Host : 06a45436b32b
Host : c19b8fb304f0
```
Pour savoir sur quel machine le service tourne, il suffit d'utiliser la commande `docker service ps <nom_du_service>`:
```
[vagrant@node1 python-docker]$ docker service ps pyapp_server_1
ID             NAME               IMAGE               NODE       DESIRED STATE   CURRENT STATE            ERROR     PORTS
n42082lx9pr2   pyapp_server_1.1   python_app:latest   node1.b3   Running         Running 57 minutes ago             
xuo70lo2322a   pyapp_server_1.2   python_app:latest   node2.b3   Running         Running 37 minutes ago             
7lb89h9fe9w6   pyapp_server_1.3   python_app:latest   node3.b3   Running         Running 37 minutes ago 
```

En arrétant le démon docker sur une **node2.b3**:
```
[vagrant@node2 python-docker]$ sudo systemctl stop docker
Warning: Stopping docker.service, but it can still be activated by:
  docker.socket

[vagrant@node2 python-docker]$ systemctl status docker
● docker.service - Docker Application Container Engine
   Loaded: loaded (/usr/lib/systemd/system/docker.service; disabled; vendor preset: disabled)
   Active: inactive (dead) since Wed 2021-01-27 14:02:23 UTC; 14s ago
     Docs: https://docs.docker.com
  Process: 3676 ExecStart=/usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock (code=exited, status=0/SUCCESS)
 Main PID: 3676 (code=exited, status=0/SUCCESS)
 ```

Et voici ce que ça donne:
```
[vagrant@node1 python-docker]$ docker service ps pyapp_server_1
ID             NAME                   IMAGE               NODE       DESIRED STATE   CURRENT STATE                ERROR     PORTS
n42082lx9pr2   pyapp_server_1.1       python_app:latest   node1.b3   Running         Running about an hour ago              
keif38802k3x   pyapp_server_1.2       python_app:latest   node1.b3   Running         Running about a minute ago             
xuo70lo2322a    \_ pyapp_server_1.2   python_app:latest   node2.b3   Shutdown        Running 42 minutes ago                 
7lb89h9fe9w6   pyapp_server_1.3       python_app:latest   node3.b3   Running         Running 42 minutes ago                 
```
On voit que le service sur **node2.b3** est éteind donc les deuxième réplicat s'est lancé sur **node1.b3**


## 3) Le registre docker
### Registre
Avant tout ça il faut autoriser l'utilisation de registres en HTTP:
```
[vagrant@node1 registry]$ sudo echo -e "{\n'insecure-registries' : ['registry.b3:5000']\n}" >> /etc/docker/daemon.json
```
Puis il faut relancer le démon docker et on peut y aller.

déployez sur node1 avec une commande `docker stack`:
```
[vagrant@node1 registry]$ docker stack deploy -c docker-compose.yml registry1
Creating network registry1_default
Creating service registry1_registry

[vagrant@node1 registry]$ d service ls
ID             NAME                 MODE         REPLICAS   IMAGE        PORTS
ax3zu33hi1rc   registry1_registry   replicated   1/1        registry:2   *:5000->5000/tcp
```

Configurer les fichiers hosts:
- **node1.b3**
    ```
    [vagrant@node1 registry]$ cat /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

    127.0.0.1 centos7.localdomain

    127.0.1.1 node1.b3 node1
    192.168.20.21 node1.b3 registry.b3
    192.168.20.22 node2.b3
    192.168.20.23 node3.b3
    ```

- **node2.b3**
    ```
    [vagrant@node2 python-docker]$ cat /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

    127.0.0.1 centos7.localdomain

    127.0.1.1 node2.b3 node2
    192.168.20.21 node1.b3 registry.b3
    192.168.20.22 node2.b3
    192.168.20.23 node3.b3
    ```


- **node3.b3**
    ```
    [vagrant@node3 python-docker]$ cat /etc/hosts
    127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
    ::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

    127.0.0.1 centos7.localdomain

    127.0.1.1 node3.b3 node3
    192.168.20.21 node1.b3 registry.b3
    192.168.20.22 node2.b3
    192.168.20.23 node3.b3
    ```


#### Tester le registre et l'utiliser
Pousser l'image sur le registre:
```
[vagrant@node1 registry]$ docker tag python_app:latest registry.b3:5000/py_app/py_app:test

[vagrant@node1 registry]$ d images
REPOSITORY                       TAG       IMAGE ID       CREATED        SIZE
python_app                       latest    6c720c1c5886   21 hours ago   893MB
registry.b3:5000/py_app/py_app   test      6c720c1c5886   21 hours ago   893MB
python                           3.8       4d53664a7025   2 days ago     883MB
redis                            <none>    621ceef7494a   2 weeks ago    104MB
registry                         <none>    678dfa38fcfa   5 weeks ago    26.2MB

[vagrant@node1 registry]$ docker push registry.b3:5000/py_app/py_app:test
The push refers to repository [registry.b3:5000/py_app/py_app]
fe63b9133e22: Pushed 
83e8f413890b: Pushed 
0236dd92a81f: Pushed 
7df99d8c2ab3: Pushed 
ac9092d7b19b: Pushed 
9f5b4cdea532: Pushed 
cd702377e4e5: Pushed 
aa7af8a465c6: Pushed 
ef9a7b8862f4: Pushed 
a1f2f42922b1: Pushed 
4762552ad7d8: Pushed 
test: digest: sha256:d2c6fc101a4af4e7ed0fc60770d083f0c52f3d92ecc9b54b3f7fc7068bb79cc7 size: 2635
```

Puis modifier le `docker-compose.yml` pour utiliser la nouvelle image sur le registre:

- Fichier Avant le registre:
    ```yml
    version: '3.8'

    services:
        server_1:
            image: python_app:latest
            networks:
                - web_server_net
            ports:
                - "8888:8888"

        redis:
            image: redis
            networks:
                - web_server_net

    networks:
        web_server_net:
    ```
- Fichier après le registre:
    ```yml
    version: '3.8'

    services:
        server_1:
            image: registry.b3:5000/py_app/py_app:test
            networks:
                - web_server_net
            ports:
                - "8888:8888"

        redis:
            image: redis
            networks:
                - web_server_net

    networks:
        web_server_net:
    ```

Bon il n'y a que l'image qui a changé.

Maintenant il faut le déployer:
```
[vagrant@node1 python-docker]$ docker stack deploy -c docker-compose.yml pyapp1
Creating network pyapp1_web_server_net
Creating service pyapp1_redis
Creating service pyapp1_server_1

[vagrant@node1 python-docker]$ d service ls
ID             NAME                 MODE         REPLICAS   IMAGE                                 PORTS
9j137s2e5ddp   pyapp1_redis         replicated   1/1        redis:latest                          
qtlx0bv7fdl3   pyapp1_server_1      replicated   1/1        registry.b3:5000/py_app/py_app:test   *:8888->8888/tcp
hnbw2wi8uhdt   registry1_registry   replicated   1/1        registry:2                            *:5000->5000/tcp
```
Ça a l'air de fonctionner voyons si la page s'affiche 
```
[vagrant@node1 python-docker]$ curl node2.b3:8888
<h1>Add key</h1>
<form action="/add" method = "POST">

Key:
<input type="text" name="key" >

Value:
<input type="text" name="value" >

<input type="submit" value="Submit">
</form>

<h1>Check key</h1>
<form action="/get" method = "POST">

Key:
<input type="text" name="key" >
<input type="submit" value="Submit">
</form>

Host : d8cc44d9714c
```
Woaw ! Ça fonctionne !!

### Centraliser l'accès aux services
#### Deployer une stack traefik
1. Crée un réseau dédié à Traefik
```
[vagrant@node1 python-docker]$ docker network create --driver overlay traefik
kndinbsx6tbtz7ua7l6esfojy

[vagrant@node1 python-docker]$ docker network ls
NETWORK ID     NAME                DRIVER    SCOPE
5d8bd4dd0eaf   bridge              bridge    local
ec161cd5f6d5   docker_gwbridge     bridge    local
c5c5aa9006de   host                host      local
hw5pu63sjmy6   ingress             overlay   swarm
7197015ebb1d   none                null      local
t9rw5zmo7b2d   registry1_default   overlay   swarm
kndinbsx6tbt   traefik             overlay   swarm
```

2. Utiliser le docker-compose.yml fourni

[Le fichier en question](data/node1/traefik/docker-compose.yml)

Une fois le nom de domaine ajouté, il faut le mettre dans les fichiers `/etc/hosts/`. Voici le celui de **node3.b3** mais les autres se ressemblent:

```
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6

127.0.0.1 centos7.localdomain

127.0.1.1 node3.b3 node3
192.168.20.21 node1.b3 registry.b3 dashb.dockerswarm.tp
192.168.20.22 node2.b3 dashb.dockerswarm.tp
192.168.20.23 node3.b3 dashb.dockerswarm.tp
```

Il n'y a plus qu'a aller sur ce nom de domaine:
![capture d'écran pour montrer traekif fonctionne bien](images/traefik.png)

On remarque que c'est rediriger vers du https avec une jolie interface

3. Passer l'application Web Python derrière Traefik
[Le nouveau fichier `docker-compose.yml`](data/node1/python-docker/docker-compose_v2.yml)


### 3) Swarm Management WebUI
[le fichier docker-compose.yml](data/node1/docker-compose-swarmpit.yml)