# Outputs

## Vérifier si nginx est bien en place

[nginx.yml](nginx.yml)
```
[hans@deployedNode ~]$ curl 192.168.20.12
Hello from 10.0.2.15

```

## Vérifier si mariadb est bien fonctionnel

[mariaDB.yml](mariaDB.yml)
```
[hans@deployedNode ~]$ mysql

MariaDB [(none)]> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| DBtest             |
| mysql              |
| performance_schema |
| test               |
+--------------------+
5 rows in set (0.00 sec)
```

## Verifier les utilisateurs

[users.yml](users.yml)
```
[hans@deployedNode ~]$ cat /etc/group | grep b3
b3:x:2021:

[hans@deployedNode ~]$ cat /etc/passwd | grep louis
louis:x:1002:2021::/home/louis:/bin/bash
[hans@deployedNode ~]$ cat /etc/passwd | grep kavilan
kavilan:x:1003:2021::/home/kavilan:/bin/bash
[hans@deployedNode ~]$ cat /etc/passwd | grep william
william:x:1004:2021::/home/william:/bin/bash
```
